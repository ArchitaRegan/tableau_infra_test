
provider "aws"  {
  region                  = "eu-west-1"
  shared_credentials_file = "/Users/ArchitaRegan/.aws/credentials"
}


data "aws_ami" "aws-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


##################################################################################
# RESOURCES
##################################################################################

# NETWORKING #
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name   = "Tableau-vpc"
  cidr    = var.vpc_cidr


 azs             = var.availability
 private_subnets = var.private_subnets
 public_subnets  = var.public_subnets

  enable_nat_gateway = true
  

  tags = {
    Project = "Tableau"
  }

}


# SECURITY GROUPS #

# ELB security group

resource "aws_security_group" "elb-sg" {
 name   = "nginx_elb_sg"
 vpc_id = module.vpc.vpc_id

  #Allow HTTP from anywhere

 ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
   from_port   = 443
   to_port     = 443
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
   from_port   = 22
   to_port     = 22
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
   
}

resource "aws_security_group" "nginx-sg" {
  name   = "nginx_sg"
  vpc_id = module.vpc.vpc_id

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.elb-sg.id]
  }

  # HTTP access from the VPC//CHANGE TO DMW VPN

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups =[aws_security_group.elb-sg.id]
  }

 # HTTPS access from the VPC//CHANGE TO DMW VPN
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups =[aws_security_group.elb-sg.id]
  }

   ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.terra_bastion_sg.id]
  }

   ingress {
    from_port   = 8850
    to_port     = 8850
    protocol    = "tcp"
    security_groups = [aws_security_group.terra_bastion_sg.id]
  }

  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

}

resource "aws_alb_target_group" "alb_target_group" {
  name     = "tableau-alb-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc_id
}

resource "aws_alb" "web" {
  name = "tableau-elb"
  load_balancer_type = "application"
  subnets         = module.vpc.public_subnets
  security_groups = [aws_security_group.elb-sg.id]
  
}

resource "aws_alb_listener" "tableau_listener" {
  load_balancer_arn = aws_alb.web.arn
  port              = 80
  protocol          = "HTTP"

   default_action {
    target_group_arn = aws_alb_target_group.alb_target_group.id
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "my_tableau_instance" {
  count = 2
  target_group_arn = aws_alb_target_group.alb_target_group.id
  target_id = aws_instance.tableau_ec2_instance[count.index].id
}


resource "aws_security_group" "terra_bastion_sg" {
  name        = "tableau_bastion_sg"
  description = "Allow all inbound traffic"
  vpc_id     = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
}

# EC2 INSTANCES - Bastion


resource aws_instance "bastion_sg" {
  ami                    = data.aws_ami.aws-linux.id
  instance_type          = "t2.micro"
  key_name               = "sshkeypair"
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.terra_bastion_sg.id]
 
}

# EC2 INSTANCES - target for the loadbalancer in the private subnet

resource aws_instance "tableau_ec2_instance" {
  count = 2
  ami                    = data.aws_ami.aws-linux.id
  instance_type          = "c5.2xlarge"
  key_name               = "sshkeypair"

  root_block_device {
    volume_size = 50
  }
  
  subnet_id              = module.vpc.private_subnets[count.index]
  vpc_security_group_ids = [aws_security_group.nginx-sg.id]
  user_data              = data.template_file.user_data.rendered


  #   connection {
  #   type        = "ssh"
  #   host        = self.public_ip
  #   user        = "ec2-user"
  #   private_key = file(var.private_key_path)
  # }
  
}

data "template_file" "user_data" {
   template = "${file("user_data")}"
   vars = { 
     wd= "/home/ec2-user",
     base_url="https://raw.githubusercontent.com/tableau/server-install-script-samples/master/linux/automated-installer/"
   }
}



# ##################################################################################
# # OUTPUT
# ##################################################################################

output "aws_instance_public_dns" {
value = aws_alb.web.id
}

